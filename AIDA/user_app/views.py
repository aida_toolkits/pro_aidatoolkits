from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from user_app.models import MyUser, TokenModel
from user_app.forms import MyUserCreationForm, Security, MyUserChangeForm
from django.contrib.auth import logout
from django.contrib import messages
from django.contrib.auth import authenticate, login as auth_login
from user_app.forms import LoginForm


# Create your views here.
def index(request):
    """
    This is home view
    request index url
    """
    context = {'home_title': "AIDAtoolkit-home page"}
    return render(request, 'home_page.html', context)


def register(request):
    """
    This function is
    performed to
    register users.

    """
    context = {'form': MyUserCreationForm()}  # form send to register.html

    if request.method == 'POST':
        form = MyUserCreationForm(request.POST)  # sends the incoming request to the form

        if form.is_valid():  # if the incoming request is true
            user = form.save(commit=False)  # form save
            user.username = form.cleaned_data['username'].lower()
            user.set_password(form.cleaned_data['password1'])  # sets passwords.
            user.is_active = False

            user.save()  # the latter saves the process

            return redirect('login')
        else:
            context = {"form": form}

    return render(request, 'register.html', context)


def verify_view(request, token, id):
    print("user id:", id)
    """
   This function generates
   tokens and
    is used when
    mail is sent
    """
    verify = TokenModel.objects.filter(
        token=token,
        expired=False,
        user_id=id
    ).last()  # generates token filter

    if verify:
        verify.user.is_active = True
        verify.user.save()
        verify.expired = True
        verify.save()
        return redirect('login')
    else:
        return redirect('dashboard')


def login_user(request):
    """
        This function is
        performed to
        login users.

    """
    context = {}
    context['form'] = LoginForm()  # empty LoginForm
    if request.method == 'POST':
        form = LoginForm(request.POST)  # Login Form username and password write
        if form.is_valid():  # if the incoming request is true
            username = form.cleaned_data.get('username')  # username example: cavidan_hasanli
            password = form.cleaned_data.get('password')  # password example: *******
            user = authenticate(username=username, password=password)  # If the incoming request authenticate
            verify = MyUser.objects.filter(username=username).last()
            verify_user = TokenModel.objects.filter(user_id=verify.id).last()
            if verify_user.user.is_active:

                if user:  # If the incoming request is true
                    auth_login(request, user)  # login user

                    return redirect('dashboard')  # redirect home url

                else:
                    messages.error(
                        request, "Username or Password inValid"
                    )  # If user login is not valid error message

                    context['form'] = form

            else:
                messages.error(
                    request, "You must verify your email"
                )  # If user not verify user message error
    return render(request, 'login.html', context)


def logout_user(request):
    """
            This function is
            performed to
            logout users.

        """
    logout(request)  # user logout
    return redirect("/")


@login_required
def change_password(request):
    """

  This feature helps the
  user to change the user's
  password when they are
  active on the site
"""
    context = {"form": Security()}

    if request.method == "POST":
        form = Security(request.POST)
        if form.is_valid():
            check = request.user.check_password(form.cleaned_data.get("CurrentPassword"))
            if check:
                pas1 = form.cleaned_data.get("Newpassword")  # Change New Password
                pas2 = form.cleaned_data.get("ConfirmPassword")  # Confrim Changed password
                if pas1 == pas2:  # if match
                    request.user.set_password(form.cleaned_data.get("Newpassword"))  # set new password to user
                    request.user.save()
                    return redirect('index')
                else:
                    messages.error(request, 'ASDASD')

            else:
                context = {"form": form}

                messages.error(request, "Password Mismatch")
    return render(request, 'change_password.html', context)


def settings_user(request, id):
    """
    This feature allows the user
    to share personal
    information within the site.
    """
    user = MyUser.objects.filter(id=id).first()
    context = {
        'edit_user': user,
        'form': MyUserChangeForm(instance=user),

    }
    if request.method == "POST":
        form = MyUserChangeForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Settings saved successfully')
            return redirect('settings', id=id)
    return render(request, 'usr_settings.html', context)
