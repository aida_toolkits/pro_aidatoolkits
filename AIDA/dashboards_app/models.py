from django.db import models
from django.shortcuts import reverse
from teams_app.models import Calendar
from colorfield.fields import ColorField


class Post(models.Model):
    connection_calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE, null=True, blank=True)
    types = (('fun_facts', 'FUN FACTS'), ('event', 'EVENT'), ('partners_info', 'Partners INFO'))
    text = models.TextField(blank=False, null=True)
    post_image = models.ImageField(upload_to='Post_images/', default='1024.jpg', null=True, blank=True)
    set_date = models.DateTimeField(auto_now=False, null=True, blank=True)
    post_type = models.CharField(choices=types, max_length=20)

    def __str__(self):
        return f'{self.text}'

    class Meta:
        verbose_name_plural = 'Post'
        ordering = ['id']

    def get_absolute_url(self):
        return reverse("post_detail", kwargs={"id": self.id})


class Post_Comment(models.Model):
    calendar_id = models.ForeignKey(Calendar, on_delete=models.CASCADE, null=True, blank=True)
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE, null=True, blank=True)
    comment = models.TextField(max_length=500, null=True, blank=True)

    def __str__(self):
        return f'Calendar is {self.calendar_id} comment {self.comment}'


