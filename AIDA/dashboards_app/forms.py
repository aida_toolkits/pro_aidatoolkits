from django import forms
from .models import Post, Post_Comment


class PostForm(forms.ModelForm):
    post_image = forms.ImageField(widget=forms.FileInput, required=False)

    class Meta:
        model = Post
        fields = ['connection_calendar', 'text', 'post_image', 'post_type', 'set_date']


class PostComment(forms.ModelForm):
    class Meta:
        model = Post_Comment
        fields = ['comment']
