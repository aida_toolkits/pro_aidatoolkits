from django.urls import path
from .views import *
urlpatterns = [
    path('calendar/<int:id>/', calendar, name='calendar'),
    path('calendar/<int:id>/add_post/', add_post, name='add_post'),
    path('calendar/<int:id>/edit_post/<int:edit_id>/', edit_post, name='edit_post'),
    path('calendar/<int:id>/all_post/<str:year>/<str:month>/<str:day>/',all_post, name = 'all_post'),
    path('delete_post/<int:id>/', post_delete, name='post_delete'),
    path('calendar/<int:id>/post_detail/<int:post_id>/', post_detail, name='post_detail'),
    path('comment/<int:id>/date/<str:year>/<str:month>/<str:day>/', comment_save, name='comment_save'),
    path('sharing/', test_sharing, name='sharing')
]
