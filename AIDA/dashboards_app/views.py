from django.core import serializers
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
import json
import ast
from django.core.mail import send_mail
from .forms import PostForm, PostComment
from .models import Post, Post_Comment
from teams_app.models import Calendar, TeamMembers, MyUser
import facebook


def calendar(request, id):
    if not request.user.is_authenticated:
        return redirect('login')
    else:
        v = Post.objects.filter(connection_calendar_id=id)

        context = {}
        teams_id = Calendar.objects.filter(id=id).values()
        for c in teams_id:
            team_members = TeamMembers.objects.filter(team_id=c['connetion_team_id']).values()
            context['team_members'] = team_members
        data_db = v.values()
        print(data_db)

        filter_data = {}
        SetDateList = []
        for m in range(0, len(data_db)):
            allSetDate = data_db[m]['set_date']
            SetDateList.append(str(allSetDate.date()))

        for s in SetDateList:
            filter_data[s] = SetDateList.count(s)

        context["finish_data"] = filter_data
        context['calendars_id'] = id
        print('filter_data', filter_data)
        return render(request, 'calendar.html', context)


def all_post(request, id, year, month, day):
    context = {}

    if not request.user.is_authenticated:
        return redirect('login')
    else:

        context['calendars_id'] = id
        context['years'] = year
        context['months'] = month
        context['days'] = day

        teams_id = Calendar.objects.filter(id=id).values()
        for c in teams_id:
            team_members = TeamMembers.objects.filter(team_id=c['connetion_team_id']).values()
            context['team_member'] = team_members

        all_posts = Post.objects.filter(connection_calendar_id=id, set_date__year=year,
                                        set_date__month=month,
                                        set_date__day=day)
        context['all_post'] = all_posts
        posts = Post.objects.filter(connection_calendar_id=id, set_date__year=year,
                                    set_date__month=month,
                                    set_date__day=day).values()
        for p in posts:
            context['save_comment'] = Post_Comment.objects.filter(post_id_id=p['id'], calendar_id_id=id)
        if request.method == "POST":
            print("POST")

            teams_id = Calendar.objects.filter(id=id).values()
            for c in teams_id:
                team_members = TeamMembers.objects.filter(team_id=c['connetion_team_id']).values()

            if 'involve_teamlead' in request.POST:
                for c in teams_id:
                    team_members = TeamMembers.objects.filter(team_id=c['connetion_team_id']).values()
                    for t in team_members:
                        if t['member_status'] == 'Leader':
                            lead = MyUser.objects.filter(id=t['member_id']).values().last()
                            send_mail('AIDA',
                                      'Sira sizdedir buyurun sayta daxil olun',
                                      'password_reminder@aidatoolkit.ru',
                                      [lead['email']],
                                      fail_silently=False)

            if 'complete' in request.POST:
                for c in teams_id:
                    team_members = TeamMembers.objects.filter(team_id=c['connetion_team_id']).values()
                    for t in team_members:
                        if t['member_status'] != 'Leader':
                            if t['member_id'] == request.user.id:
                                team_memberss = TeamMembers.objects.filter(team_id=c['connetion_team_id']).filter(
                                    number=int(t['number']) + 1).values()
                                for m in team_memberss:
                                    lead = MyUser.objects.filter(id=m['member_id']).values().last()
                                    send_mail('AIDA',
                                              'Sira sizdedir buyurun sayta daxil olun',
                                              'password_reminder@aidatoolkit.ru',
                                              [lead['email']],
                                              fail_silently=False)

        print(context)
        return render(request, 'all_post.html', context)


def comment_save(request, id, year, month, day):
    if request.method == 'POST':
        if request.is_ajax():
            print('AJAX COMMENT')
            comment = request.POST.get('comment')
            posts = Post.objects.filter(connection_calendar_id=id, set_date__year=year,
                                        set_date__month=month,
                                        set_date__day=day).values()
            for p in posts:

                if comment:
                    cmnt_save = Post_Comment.objects.create(post_id_id=p['id'], calendar_id_id=id, comment=comment)
                    cmnt_save.save()

            return JsonResponse({'comment': comment})


def add_post(request, id):
    if not request.user.is_authenticated:
        return redirect('login')
    else:
        context = {
            'forms': PostForm(),
            'id': id
        }
        if request.method == 'POST':
            form = PostForm(request.POST or None, request.FILES or None)
            # print(form)

            if form.is_valid():
                print("Valid")
                task = form.save(commit=False)
                print(task.post_type)
                task.connection_calendar_id = id
                task.save()
                return redirect('calendar', id=id)
            else:
                context['form'] = form

        return render(request, 'post_add.html', context)


def edit_post(request, edit_id, id):
    if not request.user.is_authenticated:
        return redirect('login')
    else:
        task_data = Post.objects.filter(id=edit_id).first()
        context = {
            'edit_tasks': task_data,
            'form': PostForm(instance=task_data),
            'calendar_id': id
        }
        if request.method == "POST":
            form = PostForm(data=request.POST or None, files=request.FILES or None, instance=task_data)
            if form.is_valid():
                task = form.save(commit=False)
                task.connection_calendar_id = id
                task.save()
                # task.user_id = request.user
                return redirect('calendar', id=id)
        return render(request, 'edit_post.html', context)


def post_delete(request, id):
    if not request.user.is_authenticated:
        return redirect('login')
    else:
        task_data = Post.objects.filter(id=id).first()
        task_data.delete()
        return redirect('calendar', id=task_data.connection_calendar_id)


def post_detail(request, id, post_id):
    context = {}
    detail = Post.objects.filter(connection_calendar_id=id, id=post_id)
    context['details'] = detail
    return render(request, 'post_detail.html', context)


import requests


def test_sharing(request):
    # access_token = "EAAErNriUsZCcBAHF91XLINN9ja5RZBGk0N1EeF1YQnYPPgkIEiKH6zEPsvgy4wAO8ZBbEqSZCM9n8ok4dZCYEsWcwp2zcdt08nZBhtvbo87ZAZCiXgrJXiueyElZBB6urExr1b0069ozT00MAacQ5KtMGgORZAkf9QE8TGHlGCwXHZB1Pz2NWgIoSAGf6vikhNEWGxLQRZBprS87MQZDZD"
    # graph = facebook.GraphAPI(access_token=access_token, version="2.12")
    # graph.put_object(parent_object='me', connection_name='feed',
    #                  message='Hello, world 7')

    # requestss = requests.post("https://graph.facebook.com/me?fields={104760014541528}/feed?message=Hello Fans!&access_token={EAANLZCsMhezQBAICzmSkMjdbJadhEXTs5AkvArz5nDmtORlzUYabL9ewhZBoOpKnFWCpz3DjGZABU4ygZAXvYq5N5ty2aavKZApMseyzXg03bwqCCZCZBu2JCJ4UPsDx3e7JYqSGQ5LkWjLkaYPoJhYqFUH9NZCkiJOBoJCL7QuviYchqQPDMspSyI7dyYRkfMr8z6cduVoK6pzqsyGWOsn4lo7h29SoJrr3V5NQudTivwZDZD}")
    # print(requestss)
    return render(request, 'post_share_test.html')
