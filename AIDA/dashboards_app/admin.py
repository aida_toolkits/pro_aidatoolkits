from django.contrib import admin
from .models import Post,Post_Comment
# Register your models here.
from teams_app.models import Calendar

admin.site.register(Post)
admin.site.register(Calendar)
admin.site.register(Post_Comment)


