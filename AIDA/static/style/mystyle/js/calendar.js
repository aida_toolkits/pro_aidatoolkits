let calendarSettings = {
    date: moment().set('date', 1),
    today: moment()
  }

  const incrementMonth = () => {
    calendarSettings.date.add(1, 'Months')
    console.log(`incremented to ${calendarSettings.date}`)
    displayCalendar(calendarSettings)
  }

  const decrementMonth = () => {
    calendarSettings.date.subtract(1, 'Months')
    console.log(`decremented to ${calendarSettings.date}`)
    displayCalendar(calendarSettings)
  }

  const displayCalendar = (calendarSettings) => {

    const calendar = document.querySelector('.calendar-grid')
    const calendarTitle = calendarSettings.date.format('YYYY-MM')
    const calendarTitle_2 = calendarSettings.date.format('MMM YYYY')
    const calendarTitl = calendarSettings.date.format('MMMM')
    var daysInMonth = calendarSettings.date.endOf('Month').date()
    const firstDay = calendarSettings.date.startOf('Month').isoWeekday()

    calendar.innerHTML = ''
    calendar.innerHTML = `
                          <div class="calendar-nav"><a onclick="decrementMonth()"><i class="fas fa-caret-square-left"></i> </a></div>
                          <div class="calendar-title">${calendarTitle_2}</div>
                          <div class="calendar-nav calendar-nav__right"><a  onclick="incrementMonth()" > <i class="fas fa-caret-square-right"></i></a></div>
                          <div class="calendar-dayname">Monday</div>
                          <div class="calendar-dayname">Tuesday</div>
                          <div class="calendar-dayname">Wednesday</div>
                          <div class="calendar-dayname">Thursday</div>
                          <div class="calendar-dayname">Friday</div>
                          <div class="calendar-dayname">Saturday</div>
                          <div class="calendar-dayname">Sunday</div>
                          `
let my_Arr = []

    for (let day = 1; day <= daysInMonth; day++) {
      let calendarDay = document.createElement('a')
      console.log('day:', day)
      let data = calendarTitle + '-' + day;

      let innerUrl = '/calendar/' + calendarId + '/all_post/' + data.replace(/\-/gi, '/') + '/';
        if (day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6 || day === 7 || day === 8 || day === 9  ){
            var all_date=calendarTitle + '-' + "0"+day;

            my_Arr.push(all_date)
        }
        else{
            var all_date=calendarTitle + '-' +day;
            my_Arr.push(all_date)
        }
      let row = document.createElement('div')
      let col3 = document.createElement('div')
      let col9 = document.createElement('div')
      let color = document.createElement('div')
      let red = document.createElement('div')
      let green = document.createElement('div')
      let blue = document.createElement('div')
      let month = document.createElement('div')
      let week = document.createElement('div')
      let plan = document.createElement('div')
      var saveplan = document.createElement('div')
      $(calendarDay).attr('href',innerUrl)
      let saveplans = document.createElement('div')
      var yuxarda=document.querySelector('.calendar-timme');
      yuxarda.innerHTML=calendarTitle_2;


      if (day === 1) {
        calendarDay.setAttribute('style', `grid-column-start:${firstDay}`)
        console.log(`firstDay = ${firstDay}`)
      }

      calendarDay.classList.add('calendar-table')
      row.classList.add('row')
      col3.classList.add("col-3", "col-md-3", "col-lg-3", "col-xl-3")
      col9.classList.add("col-9", "col-md-9", "col-lg-9", "col-xl-9", "mt-2","pr-0","pl-2")
      color.classList.add('calendar-color')
      red.classList.add('calendar-red')
      green.classList.add('calendar-green')
      blue.classList.add('calendar-blue')
      month.classList.add('month-week')
      week.classList.add('week-month')
      plan.classList.add('div-calendar-plan','mb-2')
      saveplan.classList.add('save-plan')
      saveplans.classList.add('save-plans')

      if (calendarSettings.today.month() == calendarSettings.date.month() && calendarSettings.today.year() == calendarSettings.date.year()) {
        if (calendarSettings.today.date() == day) {
          calendarDay.classList.add('current-day')
        }
      }
      var value = JSON.parse(document.getElementById('hello-data').textContent)
      Object.keys(value).forEach(function (key) {
                    var my_key = key
                    var my_value = value[key]
                    if (my_key === all_date){
                        saveplan.innerHTML = my_value+ ' ' +'publications'
                    }


                })

      month.innerHTML = calendarTitl+ day
        plan.innerHTML = 'Plans'

      saveplans.innerHTML = ''
      var modalMouth=document.querySelector('.modal-2-mounth');
      modalMouth.innerHTML=calendarTitle;

      calendarDay.appendChild(row)
      row.appendChild(col3)
      row.appendChild(col9)
      col3.appendChild(color)
      color.appendChild(red)
      color.appendChild(green)
      color.appendChild(blue)
      col9.appendChild(month)
      col9.appendChild(week)
      col9.appendChild(plan)
      col9.appendChild(saveplan)
      col9.appendChild(saveplans)
      calendar.appendChild(calendarDay)


    }
  }



  displayCalendar(calendarSettings);

  $('.edit-uppload-calendar').click(function(){ $('#imgupload').trigger('click'); });
