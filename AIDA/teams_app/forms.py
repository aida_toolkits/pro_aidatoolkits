from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.contrib.auth import authenticate
from user_app.models import MyUser
from teams_app.models import Team, TeamMembers


class DateInput(forms.DateInput):
    input_type = 'date'


class CreateTeam(forms.ModelForm):
    team_picture = forms.ImageField(widget=forms.FileInput, required=True)

    start_time = forms.DateTimeField(required=True, widget=DateInput)
    end_time = forms.DateTimeField(required=True, widget=DateInput)

    class Meta:
        model = Team
        fields = ['team_name', 'team_picture', 'start_time', 'end_time', 'description']


class CreateMember(forms.ModelForm):

    class Meta:
        model = TeamMembers
        fields = ['team', 'member', 'member_status', 'number']


class EditTeam(forms.ModelForm):
    team_picture = forms.ImageField(widget=forms.FileInput, required=False)

    start_time = forms.DateTimeField(required=True, widget=DateInput)
    end_time = forms.DateTimeField(required=True, widget=DateInput)

    class Meta:
        model = Team
        fields = ['team_name', 'team_picture', 'start_time', 'end_time', 'description']
